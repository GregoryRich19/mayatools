#///////////////////////////////////////////////////////////////////////////////////#
#gr_LocatorsToJoints
#created by: Greg Richardson (gscrichardson@gmail.com)
#4.20.16
#version: 1.0
#about: create customly named joints quickly with locators
#to use:  click button
#to run:  import LocatorsToJoints class and run LocatorsToJoints.display()
#return:  none
#source: none
#********************************************************************#
#notes: none
#update history: ----------
#///////////////////////////////////////////////////////////////////////////////////#

'''
INPUT TO RUN TOOL:
import gr_LocatorsToJoints as LtJ
run LtJ.display()
'''


#module imports
import maya.cmds as cmds
import maya.mel as mel


#Class
class LocatorsToJoints:
    
	#Set up global values
	LOCATORS = []
	
	JOINTS = []
	
	LOCATOR_NUM = 0
	
	WINDOW_NAME = 'locatorsToJointTool'
	    
	inputName = None
	
	@staticmethod 
	def createLocator():
	    
	    
	    #Increment locator number
	    LocatorsToJoints.LOCATOR_NUM += 1
	        
	    LocatorsToJoints.inputName = LocatorsToJoints.getInputName()
	        
	    #Store locator name
	        
	    locName = LocatorsToJoints.inputName + "_" + str(LocatorsToJoints.LOCATOR_NUM) + "_loc"
	    
	    
	    #Make the first locator
	    
	    if LocatorsToJoints.LOCATOR_NUM == 1:
	
	        #Create a locator at origin       
	        cmds.spaceLocator(name = locName)
	    
	    
	    #Make the other locators    
	    else:
	    	
	    	#Grab tranlations from previous locator
	            
	        transX = cmds.getAttr(LocatorsToJoints.LOCATORS[len(LocatorsToJoints.LOCATORS) - 1] + ".translateX")
	        transY = cmds.getAttr(LocatorsToJoints.LOCATORS[len(LocatorsToJoints.LOCATORS) - 1] + ".translateY")
	        transZ = cmds.getAttr(LocatorsToJoints.LOCATORS[len(LocatorsToJoints.LOCATORS) - 1] + ".translateZ")
	        
	        #Name locator
	        
	        cmds.spaceLocator(name = locName)
	        
	        #Start newly locators at the same location as the previous one
	            
	        cmds.setAttr(locName + ".translateX", transX)
	        cmds.setAttr(locName + ".translateY", transY)
	        cmds.setAttr(locName + ".translateZ", transZ)
	          
	        #Center the pivot of new locator
	          
	        cmds.xform(cp = True)
	
	    #Add locator to list of locators
	    LocatorsToJoints.LOCATORS.append(locName)
	    
	@staticmethod     
	def deleteLastLocator():
	
	    #Checks if there is a last locator to delete
	    #if not, displays error and resets tool
	        
	    if LocatorsToJoints.LOCATOR_NUM == 1:
	            
	        cmds.delete(LocatorsToJoints.LOCATORS[LocatorsToJoints.LOCATOR_NUM - 1])
	            
	        LocatorsToJoints.LOCATOR_NUM -= 1
	             
	        LocatorsToJoints.reset()
	            
	        cmds.error("No Locator to Delete")
	            
	    #Deletes last locator
	    cmds.delete(LocatorsToJoints.LOCATORS[LocatorsToJoints.LOCATOR_NUM - 1])
	
	    #Lowers the locator number
	    LocatorsToJoints.LOCATOR_NUM -= 1
	    
	        
	@staticmethod 
	def deleteAll():
	    #Iterates through all objects and deletes locators
	    for loc in LocatorsToJoints.LOCATORS:
	        cmds.delete(loc) 
	        
	    #Run reset function to reset values      
	    LocatorsToJoints.reset()
	        
	        
	@staticmethod 
	def reset():
	        
	    #Resets values
	    LocatorsToJoints.LOCATORS = []
	        
	    LocatorsToJoints.LOCATOR_NUM = 0
	        
	    LocatorsToJoints.JOINTS = []
	        
	@staticmethod 
	def getInputName():
	        
	    #Get the Locator's Name
	    locName = cmds.textField('inputNameTextfield',q=True,text=True)
	    return locName
	    
	@staticmethod     
	def buildJoints():
	        
	    #Deselect all items
	    
	    cmds.select( clear = True )
	
	    #Create variables to store locator attributes
	    transXAttr = 0
	    transYAttr = 0
	    transZAttr = 0
	
	    count = 0
	        
	    #Iterate through all created locators
	    for loc in LocatorsToJoints.LOCATORS:
	        count += 1
	        newName = ""
	
	        #Store all translate attribute values of a locator
	        transXAttr = cmds.getAttr(loc + ".translateX")
	        transYAttr = cmds.getAttr(loc + ".translateY")
	        transZAttr = cmds.getAttr(loc + ".translateZ")
	            
	        if "_" + str(count) + "_" in loc:
	                
	            newName = loc[:5]
	                
	
	        #Create joints from locators
	        cmds.joint(n = newName + str(count) + "_jt" ,p =(transXAttr, transYAttr, transZAttr))
	            
	        LocatorsToJoints.JOINTS.append(newName + str(count) + "_jt")
	            
	
	    #Check the checkboxes
	    LocatorsToJoints.ikOnlyCheckBoxChecked()
	    LocatorsToJoints.ikCheckBoxChecked()
	
	        
	    #Delete the locators   
	    LocatorsToJoints.deleteAll()
	    
	@staticmethod 
	def ikCheckBoxChecked():
		
		#Checks if ikCheckBox is checked
	        
	    if cmds.checkBox('ikCheckBox', query = True, value = True):
	        
	        #Checks if IK/FK and IK Only is checked, then displays error, 
	        #if not, then creates IK/FK joints
	           
	        if cmds.checkBox('ikOnlyCheckBox', query = True, value = True):
	            cmds.error("IK/FK and IK Only checkbox both checked, check only one")
	            
	        else: 
	            LocatorsToJoints.buildIKFKJoints("fk_")
	            LocatorsToJoints.buildIKFKJoints("ik_")
	                    
	                    
	@staticmethod 
	def buildIKFKJoints(name):
	        
	    #Deselect all items
	        
	    cmds.select( clear = True )
	
	    #Create variables to store locator attributes
	    transXAttr = 0
	    transYAttr = 0
	    transZAttr = 0
	
	    count = 0
	    jointCount = 0
	        
	    joints = []
	        
	    #Iterate through all created locators
	    for loc in LocatorsToJoints.LOCATORS:
	        count += 1
	        newName = ""
	
	        #Store all translate attribute values of a locator
	        transXAttr = cmds.getAttr(loc + ".translateX")
	        transYAttr = cmds.getAttr(loc + ".translateY")
	        transZAttr = cmds.getAttr(loc + ".translateZ")
	            
	        if "_" + str(count) + "_" in loc:
	                
	            newName = loc[:5]
	                
	
	        #Create joints from locators
	        cmds.joint(n = name + newName + str(count) + "_jt" ,p =(transXAttr, transYAttr, transZAttr))
	
	        joints.append(name + newName + str(count) + "_jt")
	            
	    if name == "ik_":
	        
	        #If the name is passed as "IK" then create IK handle and pass start and end joints
	        
	        LocatorsToJoints.buildIKHandle(joints[0], joints[len(joints) - 1])
	    for joint in joints:
	    	
	    	#Orient constrain all other joints to the bind joints
	    	
	        cmds.orientConstraint(joint, LocatorsToJoints.JOINTS[jointCount])
	        jointCount += 1
	            
	    LocatorsToJoints.setDrivenKeyConstraints(joints)
	            
	            
	@staticmethod    
	def buildIKHandle(start_jt, end_jt):
		
		#Build IK handle with start and end joint
		
	    cmds.ikHandle(n = start_jt + "_ik", sj= start_jt, ee= end_jt)
	       
	@staticmethod 
	def display():
	        
	    #Delete the window
	        
	    LocatorsToJoints.deleteDisplay()
	
	    #Create window
	    objWindow = cmds.window(LocatorsToJoints.WINDOW_NAME, title='Create Locators to Joints', sizeable = True, mxb = False, mnb = False)
	    
	    cmds.columnLayout(columnAttach=('both', 5), cw = 300)
	        
	    #Locators Name Row
	        
	    cmds.rowLayout(numberOfColumns = 2, cw = [(1, 100)], h = 30)
	
	    cmds.text("Name on Locator")
	        
	    cmds.textField('inputNameTextfield', enterCommand =( 'LocatorsToJoints.createLocator()'), w = 220)
	
	
	    cmds.setParent('..')
	
	    cmds.separator()
	
	    #Locator Row
	    cmds.rowLayout(numberOfColumns = 5, cw = [(1, 60), (2, 50),(3, 50),(4, 75),(5, 50)], h =  40)
	
	    cmds.text("Locators")
	    cmds.button('createLocButton',label='Create', command = "LocatorsToJoints.createLocator()")
	    cmds.button('deleteLocButton', label='Delete', command = "LocatorsToJoints.deleteLastLocator()")
	    cmds.button('deleteAllLocButton', label='Delete All', command = "LocatorsToJoints.deleteAll()")
	        
	    cmds.button('resetAllLocButton', label='Reset', command = "LocatorsToJoints.reset()")
	
	    cmds.setParent('..')
	
	    cmds.separator()
	
	    #Rig Row
	    cmds.rowLayout(numberOfColumns = 3, cw = [(1, 70), (2, 120),(3, 70)], h = 30)
	
	    cmds.text("Rig")
	    cmds.checkBox('ikCheckBox' ,label='IK/FK')
	    cmds.checkBox('ikOnlyCheckBox' ,label='IK Only')
	        
	    cmds.setParent('..')
	
	    cmds.separator()
	
	    #Joint Row
	    cmds.rowLayout(numberOfColumns = 3, cw = [(1, 60), (2, 120),(3, 70)], h = 40)
	
	    cmds.text("Joints")
	    cmds.button('buildButton', label='Build', w = 100, command = "LocatorsToJoints.buildJoints()")
	    cmds.button('mirrorJointButton', label = 'Mirror', w = 100, command = "LocatorsToJoints.openMirrorJointOptions()")
	
	    #Show window
	    cmds.showWindow(objWindow)
	
	@staticmethod 
	def deleteDisplay():
		
		#Checks if UI is displayed, if it is, UI will be deleted
		
	    if cmds.window(LocatorsToJoints.WINDOW_NAME, exists=True):
	        cmds.deleteUI(LocatorsToJoints.WINDOW_NAME, window=True)
	        
	    
	@staticmethod 
	def openMirrorJointOptions():
	    #Opens Mirror Joint Options
	    mel.eval('MirrorJointOptions;')
	        
	        