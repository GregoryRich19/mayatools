import maya.cmds as cmds

# Get the last selected curve

selected  = cmds.ls( selection=True, tail=1 )

# Duplicate curve with special name

cmds.duplicate(selected, name = "copy")

# Group duplicate with special name
        
cmds.group('copy', n ='copy_grp')

# Center pivot to origin
    
cmds.xform(piv=(0,0,0))

# Mirror across X axis

cmds.setAttr('copy_grp.scaleX', -1)

# Unparent duplicate from group

cmds.parent( 'copy_grp|copy', world = True)

# Delete group

cmds.delete('copy_grp')

# Freeze Transformation of mirror

cmds.makeIdentity(apply=True, t=1, r=1, s=1, n=0)