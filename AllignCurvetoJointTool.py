'''
Allign Curve to Joint Tool
'''

#This tool allows you to allign the control curve's orientation with the joint orientation

import maya.cmds as cmds

#Select joint then curve
#Gets last 2 previous selections

selected  = cmds.ls( selection=True, tail=2 )

#Parent constrain control curve to joint without offset to match position

cmds.parentConstraint(selected[0], selected[1], name='DeleteParentConstraint' , mo=False )

#Delete parent constraint

cmds.delete('DeleteParentConstraint')

#Group control curve

cmds.group(selected[1], name=selected[0] + '_grp')

#Zero out control curve

cmds.setAttr(selected[1] + '.rotate', 0,0,0)

#Parent group under joint

cmds.parent(selected[0] + '_grp', selected[0])

#Zero out grp rotations

cmds.setAttr(selected[0] + '_grp.rotate', 0,0,0)

#Unparent group from joint

cmds.parent(selected[0] + '|' + selected[0] + '_grp', world = True)

#Freeze Curve Translates

cmds.makeIdentity(apply=True, t=1, r=0, s=0, n=0)

#Delete curve history

cmds.delete(selected[1], ch=True)