import maya.cmds as cmds

# Select child then parent
# Gets last 2 previous selections

selected  = cmds.ls( selection=True, tail=2 )

# Parent child

cmds.parent(selected[0], selected[1])

# Zero out transforms

cmds.setAttr(selected[0] + '.translate', 0,0,0) 
cmds.setAttr(selected[0] + '.rotate', 0,0,0)

# unparent child

cmds.parent( selected[1] + "|" + selected[0], world = True)